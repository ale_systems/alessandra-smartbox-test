Set up an automated a test framework to Smartbox tests.

This project aims to test the following scenarios of the
Smartbox French Website:

Add a box to the cart;
Boxes display.

Technologies used for the tests:

- Java (any version above 1.7);
- Selenium Webdriver;
- Cucumber;
- JUnit;
- ChromeDriver;
- Eclipse IDE for Java Developers.

To run the tests you must have the following settings:

- Java JDK installed;
- Cucumber plugin;
- ChromeDriver installed.

The code was written in the Eclipse IDE for Java Developers in
Windows Environment.
Before run the tests, you must change the ChromeDriver path
to the path that you install on your machine.

The ChromeDriver path finds in the project at:

src/test/java -> steps -> AddBoxToTheCartSteps.java

src/test/java -> steps -> BoxesDisplaySteps.java

In both files in System.setProperty("webdriver.chrome.driver",
"C:\Users\Alessandra\Documents\chromedriver\chromedriver.exe");

Change the ChromeDriver path to the path  that you installed on your machine.

Install

git clone "https://gitlab.com/ale_systems/alessandra-smartbox-test.git"